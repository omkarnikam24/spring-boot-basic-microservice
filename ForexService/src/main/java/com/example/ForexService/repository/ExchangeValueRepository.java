package com.example.ForexService.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.ForexService.entity.ExchangeValue;

public interface ExchangeValueRepository extends JpaRepository<ExchangeValue, Long> {

	ExchangeValue findByFromAndTo(String from, String to);
}
