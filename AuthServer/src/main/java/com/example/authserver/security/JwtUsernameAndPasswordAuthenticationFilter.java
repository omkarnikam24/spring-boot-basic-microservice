package com.example.authserver.security;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.example.authserver.entity.AppUser;
import com.example.commonservice.security.JwtConfig;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtUsernameAndPasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private AuthenticationManager authManager;

	private final JwtConfig jwtConfig;

	public JwtUsernameAndPasswordAuthenticationFilter(AuthenticationManager authManager, JwtConfig jwtConfig) {
		super();
		this.authManager = authManager;
		this.jwtConfig = jwtConfig;

		// Override default "/login" path to "/auth" path
		this.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher(jwtConfig.getUri(), "POST"));
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		try {
			AppUser appUser = new ObjectMapper().readValue(request.getInputStream(), AppUser.class);

			UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(appUser.getUsername(),
					appUser.getPassword(), Collections.emptyList());

			return authManager.authenticate(authToken);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		Long now = System.currentTimeMillis();
		String token = Jwts.builder()
					.setSubject(authResult.getName())
					.claim("authorities", authResult.getAuthorities().stream()
							.map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
					.setIssuedAt(new Date(now))
					.setExpiration(new Date(now + jwtConfig.getExpiration() * 1000))
					.signWith(SignatureAlgorithm.HS512, jwtConfig.getSecret().getBytes())
					.compact();
		
		response.addHeader(jwtConfig.getHeader(), jwtConfig.getPrefix() + token);;
	}
}
