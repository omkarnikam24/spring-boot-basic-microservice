package com.example.authserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.authserver.entity.AppUser;

@Repository
public interface UserRepository extends JpaRepository<AppUser, Integer>{

	AppUser findAppUserByUsername(String username);
}
